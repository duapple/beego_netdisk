#!/bin/bash

cnt=0
while [ 1 ]
do
	sleep 2
	cmd=`ps | grep beego_netdisk | grep -v grep | awk '{print $4}'`
	if [ ${cmd}"A" = beego_netdisk"A" ]
	then
		cnt=0
	else
		echo "The App is quit[$cnt]!!!"
		cnt=`expr $cnt + 1`
	fi

	if [ $cnt = 3 ]
	then
		cnt=0
		echo "Restart App..."
		./beego_netdisk
	fi
done
